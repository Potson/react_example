class App extends React.Component
{
    constructor(props)
    {
        super(props)

        this.state = {
            counter: 0
        }

        console.log('Initialization')
    }

    addition()
    {
        this.setState(state => {
            return { counter: state.counter + 1 }
        })
    }

    subtraction()
    {
        this.setState(state => {
            return { counter: state.counter - 1 }
        })
    }


    componentWillMount()
    {
        console.log('Start up!')
    }


    componentDidMount()
    {
        console.log('Loaded DOM')
    }


    componentWillUnmount()
    {
        console.log('Unload component')
    }


    componentDidUpdate()
    {
        console.log('update state')
    }


    render()
    {
        console.log('Do render')

        return (
            <div className="container">
                <div className="row">
                    <h1 className="col-12 mt-3 mb-3">生命週期</h1>
                    <div className="col-5 mb-5">
                        <div className="input-group">
                            <input type="text" className="form-control" value={ this.state.counter } />
                            <div className="input-group-append">
                                <button type="button" className="btn btn-info" style={{ cursor: 'pointer' }} onClick={() => this.subtraction()}>-</button>
                                <button type="button" className="btn btn-primary" style={{ cursor: 'pointer' }} onClick={() => this.addition()}>+</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <a className="btn btn-dark" href="/">Back</a>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react_content')
);