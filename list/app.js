class App extends React.Component
{
    constructor(props)
    {
        super(props)

        this.state = {
            list: [
                { subject: '打字', order: 1 },
                { subject: '掃地', order: 2 },
                { subject: '午覺', order: 3 },
            ]
        }
    }


    list()
    {
        return this.state.list.sort((a, b) => a.order - b.order).map((params, index) => this.item(params, index))
    }


    update(column, index, value)
    {
        this.setState(state => {
            state.list[index][column] = value
            return state
        })
    }

    delete(index)
    {
        this.setState(state => {
            let newList = state.list.filter((params, itemIndex) => itemIndex == index ? false : true)
            state.list = newList
            return state
        })
    }


    componentWillMount()
    {
    }


    componentDidMount()
    {
    }


    componentWillUnmount()
    {
    }


    componentDidUpdate()
    {
    }


    item(params, index)
    {
        return (
            <div className="row mb-3">
                <div className="col">
                    <div className="form-inline">
                        <label>排序：</label>
                        <input type="number" className="form-control" value={params.order || ''} onChange={e => this.update('order', index, e.target.value)} />
                    </div>
                </div>
                <div className="col">
                    <div className="form-inline">
                        <label>項目：</label>
                        <input type="text" className="form-control" value={params.subject || ''} onChange={e => this.update('subject', index, e.target.value)} />
                    </div>
                </div>
                <div className="col">
                    <button type="button" className="btn btn-danger" style={{ cursor: 'pointer' }} onClick={() => this.delete(index)}>刪除</button>
                </div>
            </div>
        )
    }


    render()
    {
        return (
            <div className="container">
                <div className="row">
                    <h1 className="col-12 mt-3 mb-3">列表 & 資料呈現</h1>
                    <h2 className="col-12 mt-5 mb-3">工作項目</h2>
                    <div className="col-12 mb-5">
                        { this.list() }
                    </div>
                    <div className="col-12">
                        <a className="btn btn-dark" href="/">Back</a>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react_content')
);