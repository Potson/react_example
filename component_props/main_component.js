class App extends React.Component
{
    constructor(props)
    {
        super(props)

        this.state = {
            counter: 0
        }
    }

    calculate(newCounter = 0)
    {
        this.setState(state => ({ counter: state.counter + newCounter }))
    }


    componentWillMount()
    {
    }


    componentDidMount()
    {
    }


    componentWillUnmount()
    {
    }


    componentDidUpdate()
    {
    }


    render()
    {
        return (
            <div className="container">
                <div className="row">
                    <h1 className="col-12 mt-3 mb-3">元件繼承</h1>
                    <div className="col-5 mb-5">
                        <div className="input-group">
                            <input type="text" className="form-control" value={ this.state.counter } />
                            <div className="input-group-append">
                                <Sub className="btn-info" onClick={() => this.calculate(-1)} content="-" />
                                <Sub className="btn-primary" onClick={() => this.calculate(1)} content="+" />
                                {/* <button type="button" className="btn btn-primary" style={{ cursor: 'pointer' }} onClick={() => this.calculate(1)}>+</button> */}
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <a className="btn btn-dark" href="/">Back</a>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react_content')
);