class Sub extends React.Component
{
    constructor(props)
    {
        super(props)
    }


    componentWillMount()
    {
    }


    componentDidMount()
    {
    }


    componentWillUnmount()
    {
    }


    componentDidUpdate()
    {
    }


    render()
    {
        return <button type="button" className={'btn ' + this.props.className} style={{ cursor: 'pointer' }} onClick={this.props.onClick}>{this.props.content}</button>
    }
}